# NewVision

An android application for visually impaired people.
The application provides captureing image and sends it to a object detection server.
The server returns response with a list of detected object and their bounding boxes.
The application use speech to text API to talk about what is in the image.

## Author

*  [Martin Kocour](mailto:xkocou08@stud.fit.vutbr.cz)
*  Michal Gabonay
*  Klára Mihaliková
*  Tomáš Juřica