package cz.vutbr.fit.newvision.android.common

import android.content.Context
import android.support.annotation.StringRes
import cz.vutbr.fit.newvision.android.R
import java.util.*

/**
 * @author Martin Kocour
 * created on 17.10.18
 */
object SupportedObjects {
    private val mapping = HashMap<String, Int>()
    private val mapping_plurals = HashMap<String, Int>()

    init {
        run {
            mapping.put("person", R.string.object_person)
            // TODO: 17.10.18 Add more supported objects
        }
    }

    init {
        run {
            //        mapping_plurals.put("person", R.plurals.object_person_plural);
            // TODO: 17.10.18 Add more supported objects
            mapping_plurals["person"] = R.plurals.object_person_plural
            mapping_plurals["bicycle"] = R.plurals.object_bicycle_plural
            mapping_plurals["car"] = R.plurals.object_car_plural
            mapping_plurals["motorcycle"] = R.plurals.object_motorcycle_plural
            mapping_plurals["airplane"] = R.plurals.object_airplane_plural
            mapping_plurals["bus"] = R.plurals.object_bus_plural
            mapping_plurals["train"] = R.plurals.object_train_plural
            mapping_plurals["truck"] = R.plurals.object_truck_plural
            mapping_plurals["boat"] = R.plurals.object_boat_plural
            mapping_plurals["traffic light"] = R.plurals.object_traffic_light_plural
            mapping_plurals["fire hydrant"] = R.plurals.object_fire_hydrant_plural
            mapping_plurals["stop sign"] = R.plurals.object_stop_sign_plural
            mapping_plurals["parking meter"] = R.plurals.object_parking_meter_plural
            mapping_plurals["bench"] = R.plurals.object_bench_plural
            mapping_plurals["bird"] = R.plurals.object_bird_plural
            mapping_plurals["cat"] = R.plurals.object_cat_plural
            mapping_plurals["dog"] = R.plurals.object_dog_plural
            mapping_plurals["horse"] = R.plurals.object_horse_plural
            mapping_plurals["sheep"] = R.plurals.object_sheep_plural
            mapping_plurals["cow"] = R.plurals.object_cow_plural
            mapping_plurals["elephant"] = R.plurals.object_elephant_plural
            mapping_plurals["bear"] = R.plurals.object_bear_plural
            mapping_plurals["zebra"] = R.plurals.object_zebra_plural
            mapping_plurals["giraffe"] = R.plurals.object_giraffe_plural
            mapping_plurals["backpack"] = R.plurals.object_backpack_plural
            mapping_plurals["umbrella"] = R.plurals.object_umbrella_plural
            mapping_plurals["handbag"] = R.plurals.object_handbag_plural
            mapping_plurals["tie"] = R.plurals.object_tie_plural
            mapping_plurals["suitcase"] = R.plurals.object_suitcase_plural
            mapping_plurals["frisbee"] = R.plurals.object_frisbee_plural
            mapping_plurals["skis"] = R.plurals.object_skis_plural
            mapping_plurals["snowboard"] = R.plurals.object_snowboard_plural
            mapping_plurals["sports ball"] = R.plurals.object_sports_ball_plural
            mapping_plurals["kite"] = R.plurals.object_kite_plural
            mapping_plurals["baseball bat"] = R.plurals.object_baseball_bat_plural
            mapping_plurals["baseball glove"] = R.plurals.object_baseball_glove_plural
            mapping_plurals["skateboard"] = R.plurals.object_skateboard_plural
            mapping_plurals["surfboard"] = R.plurals.object_surfboard_plural
            mapping_plurals["tennis racket"] = R.plurals.object_tennis_racket_plural
            mapping_plurals["bottle"] = R.plurals.object_bottle_plural
            mapping_plurals["wine glass"] = R.plurals.object_wine_glass_plural
            mapping_plurals["cup"] = R.plurals.object_cup_plural
            mapping_plurals["fork"] = R.plurals.object_fork_plural
            mapping_plurals["knife"] = R.plurals.object_knife_plural
            mapping_plurals["spoon"] = R.plurals.object_spoon_plural
            mapping_plurals["bowl"] = R.plurals.object_bowl_plural
            mapping_plurals["banana"] = R.plurals.object_banana_plural
            mapping_plurals["apple"] = R.plurals.object_apple_plural
            mapping_plurals["sandwich"] = R.plurals.object_sandwich_plural
            mapping_plurals["orange"] = R.plurals.object_orange_plural
            mapping_plurals["broccoli"] = R.plurals.object_broccoli_plural
            mapping_plurals["carrot"] = R.plurals.object_carrot_plural
            mapping_plurals["hot dog"] = R.plurals.object_hot_dog_plural
            mapping_plurals["pizza"] = R.plurals.object_pizza_plural
            mapping_plurals["donut"] = R.plurals.object_donut_plural
            mapping_plurals["cake"] = R.plurals.object_cake_plural
            mapping_plurals["chair"] = R.plurals.object_chair_plural
            mapping_plurals["couch"] = R.plurals.object_couch_plural
            mapping_plurals["potted plant"] = R.plurals.object_potted_plant_plural
            mapping_plurals["bed"] = R.plurals.object_bed_plural
            mapping_plurals["dining table"] = R.plurals.object_dining_table_plural
            mapping_plurals["toilet"] = R.plurals.object_toilet_plural
            mapping_plurals["tv"] = R.plurals.object_tv_plural
            mapping_plurals["laptop"] = R.plurals.object_laptop_plural
            mapping_plurals["mouse"] = R.plurals.object_mouse_plural
            mapping_plurals["remote"] = R.plurals.object_remote_plural
            mapping_plurals["keyboard"] = R.plurals.object_keyboard_plural
            mapping_plurals["cell phone"] = R.plurals.object_cell_phone_plural
            mapping_plurals["microwave"] = R.plurals.object_microwave_plural
            mapping_plurals["oven"] = R.plurals.object_oven_plural
            mapping_plurals["toaster"] = R.plurals.object_toaster_plural
            mapping_plurals["sink"] = R.plurals.object_sink_plural
            mapping_plurals["refrigerator"] = R.plurals.object_refrigerator_plural
            mapping_plurals["book"] = R.plurals.object_book_plural
            mapping_plurals["clock"] = R.plurals.object_clock_plural
            mapping_plurals["vase"] = R.plurals.object_vase_plural
            mapping_plurals["scissors"] = R.plurals.object_scissors_plural
            mapping_plurals["teddy bear"] = R.plurals.object_teddy_bear_plural
            mapping_plurals["hair drier"] = R.plurals.object_hair_drier_plural
            mapping_plurals.put("toothbrush", R.plurals.object_toothbrush_plural)
        }
    }

    @JvmOverloads
    fun getString(context: Context, objectName: String, @StringRes defaultValue: Int = R.string.object_unknown): String {
        return context.getString(getString(objectName, defaultValue))
    }

    @StringRes
    fun getString(objectName: String, @StringRes defaultValue: Int): Int {
        val value = mapping[objectName]
        return value ?: defaultValue
    }

    fun getQuantitySting(appContext: Context, objectName: String, quantity: Int): String {
        val plural = mapping_plurals[objectName]
                ?: return appContext.getString(R.string.object_unknown)
        return appContext.resources.getQuantityString(plural, quantity)
    }
}
