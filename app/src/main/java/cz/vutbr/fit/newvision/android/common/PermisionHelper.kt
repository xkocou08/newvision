package cz.vutbr.fit.newvision.android.common

import android.Manifest.permission.CAMERA
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat.checkSelfPermission

/**
 * @author Martin Kocour
 * created on 18/12/2018
 */
object PermisionHelper {

    val CAMEREA_REQUEST = 9001

    fun hasCameraPermission(context: Context): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(context, CAMERA) == PERMISSION_GRANTED
    }

    fun requestCameraPermission(context: Activity) {
        ActivityCompat.requestPermissions(context, arrayOf(CAMERA), CAMEREA_REQUEST)
    }
}
