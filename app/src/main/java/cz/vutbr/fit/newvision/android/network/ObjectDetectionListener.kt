package cz.vutbr.fit.newvision.android.network

import android.os.RemoteException
import android.util.Log
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString

/**
 * @author Martin Kocour
 * created on 28/11/2018
 */
abstract class ObjectDetectionListener : WebSocketListener() {

    abstract fun onError(webSocket: WebSocket, throwable: Throwable, response: Response?)
    abstract fun onSuccess(response: ObjectDetectionResponse)

    override fun onOpen(webSocket: WebSocket, response: Response) {
        super.onOpen(webSocket, response)
        Log.d(TAG, "WebSocket: Open")
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        super.onMessage(webSocket, text)
        Log.d(TAG, "WebSocket: Receive message with content:\n" + text)
        val response = ObjectDetectionAPI.parse(text)
        Log.i(TAG, "Object detection: Processing time: " + response.processingTime!!)
        Log.i(TAG, "Object detection: Server overhead: " + response.serverOverhead!!)
        if (response.status == 0) {
            onSuccess(response)
        } else {
            Log.e(TAG, "WebSocket: Receive status " + response.status!!)
            onError(webSocket, RemoteException("Receive status: " + response.status!!), null)
        }
        webSocket.send("EOS")
    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        super.onMessage(webSocket, bytes)
        Log.d(TAG, "WebSocket: Receive bytes of size " + bytes.size())
        throw NotImplementedError("Received bytest. Don't know what to do!")
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosing(webSocket, code, reason)
        Log.d(TAG, "WebSocket: Closing with reason ($code) $reason")
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosed(webSocket, code, reason)
        Log.d(TAG, "WebSocket: Closed")
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        super.onFailure(webSocket, t, response)
        Log.e(TAG, "WebSocket: Failure", t)
        onError(webSocket, t, response)
    }

    companion object {

        private val TAG = ObjectDetectionListener::class.java.simpleName
    }
}
