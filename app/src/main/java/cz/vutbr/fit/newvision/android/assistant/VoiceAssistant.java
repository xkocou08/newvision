package cz.vutbr.fit.newvision.android.assistant;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

import cz.vutbr.fit.newvision.android.NewVisionApplication;
import cz.vutbr.fit.newvision.android.common.SupportedObjects;
import cz.vutbr.fit.newvision.android.network.ObjectDetectionResponse;

/**
 * @author Martin Kocour
 * created on 8.10.18
 */
public class VoiceAssistant implements TextToSpeech.OnInitListener {

    private static final String TAG = VoiceAssistant.class.getSimpleName();
    private enum Location {
        LEFT,
        MIDDLE,
        RIGHT,
        FRONT
    }
    private enum SECTION {
        A_SECT,
        B_SECT,
        C_SECT,
        D_SECT,
        E_SECT,
        F_SECT
    }
    private static final float AREA_TRASHHOLD = 100; // 1000
    private static final float PROBABILITY_TRASHHOLD = 0.5f; // 0.7f; // 0.8f
    private static final float image_width = 416;


    private TextToSpeech tts;
    private int total_objects = 0;
    private Context context = NewVisionApplication.Companion.getAppContext();

    private Boolean isReady = false;
    private Queue<String> msgQueue = new ArrayBlockingQueue<>(10);

    public VoiceAssistant(Context context) {
        tts = new TextToSpeech(context, this);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.ERROR) {
            Log.w(TAG, "Could not initialize TTS engine!");
        } else {
            tts.setLanguage(Locale.UK);
            tts.setPitch(0.9f);
            tts.setSpeechRate(0.8f);
            isReady = true;
            for (String msg: msgQueue) {
                speakOutput(msg);
            }
        }
    }

    public String processDetectedObjects(ObjectDetectionResponse detectedObjects) {
        String output = "There";
        String objects_string = "";

        HashMap<String, Integer> left_objects = new HashMap<String, Integer>();
        HashMap<String, Integer> right_objects = new HashMap<String, Integer>();
        HashMap<String, Integer> middle_objects = new HashMap<String, Integer>();
        HashMap<String, Integer> front_objects = new HashMap<String, Integer>();

        for (ObjectDetectionResponse.DetectedObject object: detectedObjects.getResult()) {
            if (getAreaOfObject(object) > AREA_TRASHHOLD && object.getProbability() > PROBABILITY_TRASHHOLD){
                Location object_location = getLocationOfObject(object);
                switch (object_location) {
                    case LEFT:
                        if (left_objects.get(object.getName()) != null){
                            left_objects.put(object.getName(), left_objects.get(object.getName()) + 1);
                        } else {
                            left_objects.put(object.getName(), 1);
                        }
                        break;
                    case MIDDLE:
                        if (middle_objects.get(object.getName()) != null){
                            middle_objects.put(object.getName(), middle_objects.get(object.getName()) + 1);
                        } else {
                            middle_objects.put(object.getName(), 1);
                        }
                        break;
                    case RIGHT:
                        if (right_objects.get(object.getName()) != null){
                            right_objects.put(object.getName(), right_objects.get(object.getName()) + 1);
                        } else {
                            right_objects.put(object.getName(), 1);
                        }
                        break;
                    default:
                        if (front_objects.get(object.getName()) != null){
                            front_objects.put(object.getName(), front_objects.get(object.getName()) + 1);
                        } else {
                            front_objects.put(object.getName(), 1);
                        }
                }
            }
        }

        if (left_objects.isEmpty() && right_objects.isEmpty() && middle_objects.isEmpty() && front_objects.isEmpty()){
            output += " is nothing important in front of you.";
        } else {
            objects_string += createObjectsStringByLocation(left_objects, Location.LEFT);
            objects_string += createObjectsStringByLocation(right_objects, Location.RIGHT);
            objects_string += createObjectsStringByLocation(middle_objects, Location.MIDDLE);
            objects_string += createObjectsStringByLocation(front_objects, Location.FRONT);

            if (this.total_objects == 1) {
                output += " is ";
            } else if (this.total_objects > 1){
                output += " are ";
            }
            output += objects_string;
            output = output.substring(0, output.length()-2) + ".";
        }

        return output;
    }

    public void speakOutput(String output) {
        if (!isReady) {
            msgQueue.add(output);
            return;
        }
        tts.speak(output, TextToSpeech.QUEUE_FLUSH, null, UUID.randomUUID().toString());
    }

    public void close() {
        tts.shutdown();
    }

    private Location getLocationOfObject(ObjectDetectionResponse.DetectedObject object){
        SECTION min_sect = getSection(object.getBox().x1());
        SECTION max_sect = getSection(object.getBox().x2());

        if ((min_sect == SECTION.A_SECT || min_sect == SECTION.B_SECT)
                && (max_sect == SECTION.A_SECT || max_sect == SECTION.B_SECT || max_sect == SECTION.C_SECT )) {
            return Location.LEFT;
        } else if ((min_sect == SECTION.B_SECT && (max_sect == SECTION.D_SECT || max_sect == SECTION.E_SECT))
                || (min_sect == SECTION.C_SECT && (max_sect == SECTION.C_SECT || max_sect == SECTION.D_SECT || max_sect == SECTION.E_SECT))
                || (min_sect == SECTION.D_SECT && max_sect == SECTION.D_SECT)){
            return Location.MIDDLE;
        } else if ((min_sect == SECTION.D_SECT && (max_sect == SECTION.E_SECT || max_sect == SECTION.F_SECT))
                || (min_sect == SECTION.E_SECT && (max_sect == SECTION.E_SECT || max_sect == SECTION.F_SECT))
                || (min_sect == SECTION.F_SECT && max_sect == SECTION.F_SECT)){
            return Location.RIGHT;
        } else {
            return Location.FRONT;
        }
    }

    private float getAreaOfObject(ObjectDetectionResponse.DetectedObject object){

        return object.getBox().getWidth() * object.getBox().getHeight();
    }

    private SECTION getSection(float point){

        if (point < image_width/6){
            return SECTION.A_SECT;
        } else if (point >= image_width/6 && point < image_width/3){
            return SECTION.B_SECT;
        } else if (point >= image_width/3 && point < image_width/2) {
            return SECTION.C_SECT;
        } else if (point >= image_width/2 && point < 2*image_width/3) {
            return SECTION.D_SECT;
        } else if (point >= 2*image_width/3 && point < 5*image_width/6) {
            return SECTION.E_SECT;
        } else if (point >= 5*image_width/6 && point <= image_width) {
            return SECTION.F_SECT;
        } else {
            throw new IllegalArgumentException("Bounding box out of Image!");
        }
    }

    private String createObjectsStringByLocation(HashMap<String, Integer> objects, Location location){
        StringBuilder location_string = new StringBuilder();
        int loc_count = 0;
        int index = 0;

        Set<Map.Entry<String, Integer>> set = objects.entrySet();
        Iterator<Map.Entry<String, Integer>> i = set.iterator();
        while(i.hasNext()) {
            Map.Entry<String, Integer> me = i.next();
            this.total_objects += me.getValue();
            loc_count += me.getValue();
            index++;
            location_string.append(me.getValue().toString());
            location_string.append(" ");
            location_string.append(SupportedObjects.INSTANCE.getQuantitySting(context, me.getKey(), me.getValue()));

            if (index == objects.size() - 1) {
                location_string.append(" and ");
            } else if (index == objects.size()){
                location_string.append(" ");
            } else {
                location_string.append(", ");
            }

        }
        if (loc_count > 0) {

            switch (location) {
                case LEFT:
                    location_string.append("on the left, ");
                    break;
                case RIGHT:
                    location_string.append("on the right, ");
                    break;
                case MIDDLE:
                    location_string.append("in the middle, ");
                    break;
                default:
                    location_string.append("in the front, ");
                    break;
            }
        }
        return String.valueOf(location_string);
    }

}
