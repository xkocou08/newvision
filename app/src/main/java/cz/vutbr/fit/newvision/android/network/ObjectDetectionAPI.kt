package cz.vutbr.fit.newvision.android.network


import android.content.SharedPreferences
import android.graphics.Bitmap
import android.preference.PreferenceManager
import android.util.Log
import com.google.gson.Gson
import cz.vutbr.fit.newvision.android.NewVisionApplication
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.ByteString
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

/**
 * @author Martin Kocour
 * created on 28/11/2018
 */
class ObjectDetectionAPI private constructor() {

    val client: OkHttpClient
    val gson: Gson
    private val requestId = AtomicInteger(0)
    private val preferences: SharedPreferences

    private val url: String
        get() {
            val id = requestId.incrementAndGet()
            requestId.set(id)
            return String.format(Locale.US, URL_FORMAT, host, port)
        }

    private val port: Int?
        get() {
            val port = preferences.getString("portNo", null)
            return if (port == null) DEFAULT_PORT else Integer.valueOf(port)
        }

    private val host: String
        get() = preferences.getString("hostName", DEFAULT_HOST)

    init {
        client = OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .build()
        gson = Gson()
        preferences = PreferenceManager.getDefaultSharedPreferences(NewVisionApplication.appContext)
    }

    companion object {

        private val TAG = ObjectDetectionAPI::class.java.simpleName

        private val URL_FORMAT = "ws://%s:%d/client/ws/video"
        //    private static final String DEFAULT_HOST = "baguette.hi.inet";
        private val DEFAULT_HOST = "ec2-54-88-163-166.compute-1.amazonaws.com" //"baguette.hi.inet";
        private val DEFAULT_PORT = 8082


        private val FRAME_MAX_WIDTH = 416 //600 in TF
        private val FRAME_MAX_HEIGHT = 416 //600 in TF

        private var instance = ObjectDetectionAPI()

        fun callDetect(frame: Bitmap, frameId: Int, listener: ObjectDetectionListener) {
            Log.d(TAG, "Call object detection")

            val pWidth = frame.width
            val pHeight = frame.height
            var tWidth = FRAME_MAX_WIDTH
            var tHeight = FRAME_MAX_HEIGHT
            if (pWidth > pHeight) {
                tHeight = (pHeight * (tWidth.toFloat() / pWidth)).toInt()
            } else {
                tWidth = (pWidth * (tHeight.toFloat() / pHeight)).toInt()
            }
            val data = Bitmap.createScaledBitmap(frame, tWidth, tHeight, false)
            val baos = ByteArrayOutputStream()
            data.compress(Bitmap.CompressFormat.JPEG, 100, baos)

            val frameIdBytes = ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN)
            frameIdBytes.putInt(0)
            frameIdBytes.putInt(frameId)
            val options = ByteArray(8)

            val payload = ByteBuffer.allocate(16 + baos.size())
            payload.put(frameIdBytes.array())
            payload.put(options)
            payload.put(baos.toByteArray())

            val request = Request.Builder()
                    .url(instance.url)
                    .build()
            val ws = instance.client.newWebSocket(request, listener)
            ws.send(ByteString.of(*payload.array()))
            ws.send("EMPTY")

            Log.d(TAG, "Data of size: " + baos.size() + " has been sent")
        }

        internal fun parse(json: String): ObjectDetectionResponse {
            return instance.gson.fromJson(json, ObjectDetectionResponse::class.java)
        }
    }
}
