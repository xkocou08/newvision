package cz.vutbr.fit.newvision.android.fragment


import android.os.Bundle
import android.preference.PreferenceFragment

import cz.vutbr.fit.newvision.android.R

class SettingsFragment : PreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)
    }

}
