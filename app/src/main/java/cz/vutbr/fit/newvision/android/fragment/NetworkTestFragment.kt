package cz.vutbr.fit.newvision.android.fragment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cz.vutbr.fit.newvision.android.R
import cz.vutbr.fit.newvision.android.assistant.VoiceAssistant
import cz.vutbr.fit.newvision.android.network.ObjectDetectionAPI
import cz.vutbr.fit.newvision.android.network.ObjectDetectionListener
import cz.vutbr.fit.newvision.android.network.ObjectDetectionResponse
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_network_test.*
import okhttp3.Response
import okhttp3.WebSocket

/**
 * @author Martin Kocour
 * created on 31.10.18
 */
class NetworkTestFragment : Fragment() {

    private val TAG = NetworkTestFragment::class.simpleName

    private lateinit var voiceAssistant : VoiceAssistant

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        voiceAssistant = VoiceAssistant(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_network_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        image.setImageBitmap(BitmapFactory.decodeStream(context?.assets?.open("office.jpg")))
        ok.setOnClickListener {
            detect()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        clearFindViewByIdCache()
    }

    fun detect() {
        val frame: Bitmap
        if (image.drawable is BitmapDrawable) {
            frame = (image.drawable as BitmapDrawable).bitmap
        } else {
            frame =  BitmapFactory.decodeStream(context?.assets?.open("office.jpg"))
        }
        ObjectDetectionAPI.callDetect(frame, 1, object : ObjectDetectionListener() {

            override fun onSuccess(response: ObjectDetectionResponse) {
                activity?.runOnUiThread {
                    val speech = voiceAssistant.processDetectedObjects(response)
                    text.text = speech
                    voiceAssistant.speakOutput(speech)
                }
            }

            override fun onError(webSocket: WebSocket, throwable: Throwable, response: Response?) {
                Log.e(TAG, "Error occured while detecting objects", throwable)
                throwable.printStackTrace()
                activity?.runOnUiThread {
                    Toast.makeText(context, R.string.network_unavailable, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}