package cz.vutbr.fit.newvision.android

import android.app.Application
import android.content.Context

/**
 * @author Martin Kocour
 * created on 23.10.18
 */
class NewVisionApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this
    }

    companion object {

        lateinit var instance: NewVisionApplication
            protected set

        val appContext: Context
            get() = instance.applicationContext
    }
}
