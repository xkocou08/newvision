package cz.vutbr.fit.newvision.android.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import cz.vutbr.fit.newvision.android.R
import cz.vutbr.fit.newvision.android.assistant.VoiceAssistant
import cz.vutbr.fit.newvision.android.network.ObjectDetectionResponse

/**
 * @author Martin Kocour
 * created on 8.10.18
 */
class VoiceTestFragment : Fragment() {

    internal lateinit var b1: Button
    internal lateinit var o1: TextView

    internal lateinit var tts: VoiceAssistant

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b1 = view.findViewById<Button>(R.id.buttonOutput)
        o1 = view.findViewById<TextView>(R.id.DetectedObjects)

        tts = VoiceAssistant(context)

        b1.setOnClickListener {
            val toSpeak = tts.processDetectedObjects(ObjectDetectionResponse.makeTestObject())
            Toast.makeText(context, toSpeak, Toast.LENGTH_LONG).show()
            tts.speakOutput(toSpeak)
            o1.text = toSpeak
        }
    }
}