package cz.vutbr.fit.newvision.android.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import cz.vutbr.fit.newvision.android.R
import cz.vutbr.fit.newvision.android.fragment.SettingsFragment

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setupActionBar()

        fragmentManager.beginTransaction().replace(R.id.settingsContent, SettingsFragment()).commit()
    }

    private fun setupActionBar(){
        val actionBar = supportActionBar
        actionBar!!.title = resources.getString(R.string.settings)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

}
