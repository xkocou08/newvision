package cz.vutbr.fit.newvision.android.network

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author Martin Kocour
 * created on 17.10.18
 */
class ObjectDetectionResponse {

    var status: Int? = STATUS_SUCCESS

    @SerializedName("request_id")
    var requestId = "1" // Unique id

    var frame: Int? = 1 // Frame sequence number

    var result: MutableList<DetectedObject> = mutableListOf()

    @SerializedName("total_objects")
    var totalObjects: Int? = 0

    @SerializedName("server_overhead")
    var serverOverhead: Float? = .0f

    @SerializedName("processing_time")
    var processingTime: Float? = .0f

    class DetectedObject {
        var name = ""

        @SerializedName("prob")
        var probability: Float? = .0f

        @SerializedName("bbox")
        var box = BoundingBox()

        constructor() {}

        constructor(name: String, probability: Float, box: BoundingBox) {
            this.name = name
            this.probability = probability
            this.box = box
        }
    }

    class BoundingBox {
        @SerializedName(value = "x", alternate = arrayOf("x1", "xmin"))
        private var x1 = .0f
        @SerializedName(value = "y", alternate = arrayOf("y1", "ymin"))
        private var y1 = .0f
        @SerializedName(value = "x2", alternate = arrayOf("xmax"))
        private var x2 = .0f
        @SerializedName(value = "y2", alternate = arrayOf("ymax"))
        private var y2 = .0f

        @SerializedName(value = "w", alternate = arrayOf("width"))
        private var width = .0f
        @SerializedName(value = "h", alternate = arrayOf("height"))
        private var height = .0f

        constructor() {}

        constructor(x1: Float, y1: Float, x2: Float, y2: Float) {
            this.x1 = x1
            this.x2 = x2
            this.y1 = y1
            this.y2 = y2
        }

        fun x1(): Float? {
            return if (x2 > 0f) x1 else x1 - width / 2
        }

        fun x2(): Float? {
            return if (x2 > 0f) x2 else x1 + width / 2
        }

        fun y1(): Float? {
            return if (y2 > 0f) y1 else y1 - height / 2
        }

        fun y2(): Float? {
            return if (y2 > 0f) y2 else y1 + height / 2
        }

        fun getWidth(): Float? {
            return if (width > 0f) width else Math.abs(x2 - x1)
        }

        fun getHeight(): Float? {
            return if (height > 0f) height else Math.abs(y2 - y1)
        }
    }

    companion object {
        val STATUS_EOS = -1
        val STATUS_SUCCESS = 0
        val STATUS_NO_SPEECH = 1
        val STATUS_ABORTED = 2
        val STATUS_AUDIO_CAPTURE = 3
        val STATUS_NETWORK = 4
        val STATUS_NOT_ALLOWED = 5
        val STATUS_SERVICE_NOT_ALLOWED = 6
        val STATUS_BAD_GRAMMAR = 7
        val STATUS_LANGUAGE_NOT_SUPPORTED = 8
        val STATUS_NOT_AVAILABLE = 9

        fun makeTestObject(): ObjectDetectionResponse {
            val bbox1 = BoundingBox(35f, 35f, 70f, 70f)
            val bbox2 = BoundingBox(90f, 35f, 125f, 70f)
            val bbox3 = BoundingBox(300f, 35f, 350f, 70f)
            val object1 = DetectedObject("person", 0.9f, bbox1)
            val object2 = DetectedObject("person", 0.2f, bbox2)
            val object3 = DetectedObject("cat", 0.8f, bbox2)
            val object4 = DetectedObject("cat", 0.8f, bbox3)
            val test = ObjectDetectionResponse()
            test.result = ArrayList()
            test.result.add(object1)
            test.result.add(object2)
            test.result.add(object3)
            test.result.add(object4)
            return test
        }
    }
}
